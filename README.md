# my url shortening

time start is Saturday May 29, 2021, 8:12PM

quick start instructions

```
python3 -m venv venv
source venv/bin/activate  # On Windows use `env\Scripts\activate`
pip install -r requirements.txt
echo "from django.contrib.auth import get_user_model; User = get_user_model(); User.objects.create_superuser('admin', 'admin@myproject.com', 'admin123')" | python manage.py shell
python manage.py makemigrations
python manage.py migrate

```

run unit tests
```
python manage.py test
```

run management command to add random 6 char skinny url
```
python manage.py add_skinny_urls
```

run web server
```
python manage.py runserver
```

it is now Sunday, May 30, 2021 12:07 AM.
i did about 3 hours 
