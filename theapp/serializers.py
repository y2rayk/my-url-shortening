from .models import SkinnyURL
from rest_framework_json_api import serializers


class SkinnyURLSerializer(serializers.ModelSerializer):
    class Meta:
        model = SkinnyURL
        fields = ['id', 'owner', 'target', ]
