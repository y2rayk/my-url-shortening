from django.db import models
from django.conf import settings

class SkinnyURL(models.Model):
    id = models.CharField(max_length=6, primary_key=True)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT, related_name='sender', null=True)
    target = models.CharField(max_length=2000, null=True)
