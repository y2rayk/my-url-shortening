from rest_framework_json_api.views import viewsets
from rest_framework import permissions
from .models import SkinnyURL
from .serializers import SkinnyURLSerializer
from rest_framework.response import Response


class SkinnyURLViewSet(viewsets.ModelViewSet):
    queryset = SkinnyURL.objects.exclude(owner__isnull=True)
    serializer_class = SkinnyURLSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_permissions(self):
        if self.action == 'retrieve':
            permission_classes = []
        else:
            permission_classes = [permissions.IsAuthenticated]
        return [permission() for permission in permission_classes]
