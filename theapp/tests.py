from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from .models import SkinnyURL
from django.contrib.auth import get_user_model
from collections import OrderedDict
from rest_framework.exceptions import ErrorDetail
from unittest import mock
USER_MODEL = get_user_model()

class SkinnyURLTests(APITestCase):

    def setUp(self):
        jack = self.create_user('jack')
        jill = self.create_user('jill')

    def test_unauthenticated_should_403_on_POST(self):
        url = reverse('skinnyurl-list')
        data = {'name': 'DabApps'}
        response = self.client.post(url, data, format='vnd.api+json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_unauthenticated_should_404_on_GET(self):
        url = reverse('skinnyurl-list')
        response = self.client.get(f'{url}ffff/', format='vnd.api+json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_unauthenticated_should_200_on_GET(self):
        self.create_skinnyurl('asdf', 'jack')
        url = reverse('skinnyurl-list')
        response = self.client.get(f'{url}asdf/', format='vnd.api+json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_unauthenticated_should_403_on_DELETE(self):
        url = reverse('skinnyurl-list')
        response = self.client.delete(url, format='vnd.api+json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_unauthenticated_should_403_on_PATCH(self):
        url = reverse('skinnyurl-list')
        data = {'name': 'DabApps'}
        response = self.client.patch(url, data, format='vnd.api+json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_create_skinnyurl_should_create_obj(self):
        self.create_skinnyurl('asdf')
        jack = USER_MODEL.objects.get(username='jack')
        self.client.force_authenticate(user=jack)
        url = reverse('skinnyurl-list')
        data = {'target': 'http://example.com'}
        response = self.client.post(url, data, format='vnd.api+json')
        1/0

    def test_create_skinnyurl_should_error_when_no_URL_available(self):
        1/0

    def test_fetch_non_existing_skinnyurl_should_error(self):
        jack = USER_MODEL.objects.get(username='jack')
        self.client.force_authenticate(user=jack)
        url = reverse('skinnyurl-list')
        response = self.client.get(f'{url}dontcare/', format='vnd.api+json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_fetch_unclaimed_skinnyurl_should_error(self):
        self.create_skinnyurl('asdf')
        jack = USER_MODEL.objects.get(username='jack')
        self.client.force_authenticate(user=jack)
        url = reverse('skinnyurl-list')
        response = self.client.get(f'{url}asdf/', format='vnd.api+json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_fetch_only_my_skinnyurls(self):
        1/0

    def test_fetch_one_skinnyurl_should_have_many_fields(self):
        self.create_skinnyurl('asdf', 'jack', 'http://example.com')
        url = reverse('skinnyurl-list')
        response = self.client.get(f'{url}asdf/', format='vnd.api+json')
        expected = {
            'id': 'asdf',
            'owner': OrderedDict([('type', 'User'), ('id', '1')]),
            'target': 'http://example.com'
        }
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, expected)

    def test_delete_skinnyurl(self):
        1/0

    def test_delete_skinnyurl_by_nonowner_should_errpr(self):
        1/0

    def test_delete_non_existing_skinnyurl_should_error(self):
        jack = USER_MODEL.objects.get(username='jack')
        self.client.force_authenticate(user=jack)
        url = reverse('skinnyurl-list')
        response = self.client.delete(f'{url}DNE/', format='vnd.api+json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def create_user(self, username):
        user = USER_MODEL.objects.create(username=username, password='password')

    def create_skinnyurl(self, pk, username=None, target=None):
        owner = USER_MODEL.objects.get(username=username) if username else None
        SkinnyURL(id=pk, owner=owner, target=target).save()
