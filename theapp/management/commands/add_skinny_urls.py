from django.core.management.base import BaseCommand, CommandError
import random
import string
from theapp.models import SkinnyURL
from django.db import IntegrityError


class Command(BaseCommand):
    help = 'some help'

    def generate_random_key(self):
        letters = string.ascii_lowercase
        the_key = ''.join(random.choice(letters) for i in range(6))
        return the_key

    def handle(self, *args, **options):
        for x in range(10):
            try:
                key = self.generate_random_key()
                SkinnyURL.objects.create(id=key)
                print(f'created new key? {key}')
                break
            except IntegrityError:
                print('key already exists!')
        else:
            assert True, "couldnt create key after many times!"
