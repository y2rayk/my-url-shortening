# Generated by Django 3.2.3 on 2021-05-30 01:48

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='SkinnyURL',
            fields=[
                ('id', models.CharField(max_length=6, primary_key=True, serialize=False)),
                ('target', models.CharField(max_length=2000)),
                ('owner', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='sender', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
